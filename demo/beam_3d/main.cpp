// Copyright (C) 2006-2011 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2011-02-06

#include <dolfin.h>
#include <FenicsSolidMechanics.h>
#include "../forms/p2_forms/Plas3D.h"

using namespace dolfin;

// Right-hand side (body force)
class Source : public Expression
{
public:

  Source(const double& t) : Expression(3), t(t) {}

  void eval(Array<double>& values, const Array<double>& x) const
  {
    // Body force
    values[0] =  0.0;
    values[1] =  0.0;
    values[2] = -40.0*sin((t/2.0)*DOLFIN_PI);
  }
  const double& t;
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundaryXY : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    // Locate the line (0, 0, 0.5) - (0, 1, 0.5)
    return (x[0] < DOLFIN_EPS && std::abs(x[2] - 0.5) < DOLFIN_EPS);
  }
};


// Sub domain for Dirichlet boundary condition
class DirichletBoundaryZ : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    // Locate the lines (0, 0, 0.5) - (0, 1, 0.5) and (5, 0, 0.5) - (5, 1, 0.5)
    return (x[0] < DOLFIN_EPS && std::abs(x[2] - 0.5) < DOLFIN_EPS)
                   || (std::abs(x[0] - 5.0) < DOLFIN_EPS
                       && std::abs(x[2] - 0.5) < DOLFIN_EPS);
  }
};

int main()
{
  Timer timer("Total plasicity solver time");

  Mesh mesh("beam10000.xml.gz");

  // Young's modulus and Poisson's ratio
  double E = 20000.0;
  double nu = 0.3;

  // Final time and time step
  const double T = 1.0;
  double dt = 0.1;
  double t = 0.0;

  // Source term, RHS
  Source f(t);

  // Function spaces
  Plas3D::FunctionSpace V(mesh);
  dolfin::cout << "Number of dofs: " << V.dim() << dolfin::endl;

  // Extract elements for stress and tangent
  std::shared_ptr<const FiniteElement> element_t;
  std::shared_ptr<const FiniteElement> element_s;
  {
    Plas3D::BilinearForm::CoefficientSpace_t Vt(mesh);
    element_t = Vt.element();
  }

  Plas3D::LinearForm::CoefficientSpace_s Vs(mesh);
  element_s = Vs.element();

  // Create boundary conditions (use SubSpace to apply simply
  // supported BCs)
  SubSpace V0(V, 0);
  SubSpace V1(V, 1);
  SubSpace V2(V, 2);

  Constant zero(0.0);
  DirichletBoundaryXY dbXY;
  DirichletBoundaryZ dbZ;
  DirichletBC bc0(V0, zero, dbXY, "pointwise");
  DirichletBC bc1(V1, zero, dbXY, "pointwise");
  DirichletBC bc2(V2, zero, dbZ,  "pointwise");

  std::vector<const DirichletBC*> bcs;
  bcs.push_back(&bc0);
  bcs.push_back(&bc1);
  bcs.push_back(&bc2);

  // Slope of hardening (linear) and hardening parameter
  const double E_t = 0.3*E;
  const double hardening_parameter = E_t/(1.0 - E_t/E);

  // Yield stress
  const double yield_stress = 200.0;

  // Solution function
  Function u(V);

  // Object of class von Mises
  const fenicssolid::VonMises J2(E, nu, yield_stress, hardening_parameter);

  // Constituive update
  std::shared_ptr<fenicssolid::ConstitutiveUpdate>
  constitutive_update(new fenicssolid::ConstitutiveUpdate(u, *element_s,
                                                            *Vs.dofmap(), J2));

  // Create forms and attach functions
  fenicssolid::QuadratureFunction tangent(mesh, element_t,
                                          constitutive_update,
                                          constitutive_update->w_tangent());

  Plas3D::BilinearForm a(V, V);
  a.t = tangent;

  Plas3D::LinearForm L(V);
  L.f = f;
  fenicssolid::QuadratureFunction stress(mesh, element_s,
                                         constitutive_update->w_stress());
  L.s = stress;

  // Create PlasticityProblem
  fenicssolid::PlasticityProblem nonlinear_problem(a, L, u, tangent,
                                                   stress, bcs, J2);

  // Create nonlinear solver and set parameters
  dolfin::NewtonSolver nonlinear_solver;
  nonlinear_solver.parameters["convergence_criterion"] = "incremental";
  nonlinear_solver.parameters["maximum_iterations"]    = 50;
  nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
  nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

  // File names for output
  File file1("output/disp.pvd");
  File file2("output/eq_plas_strain.pvd");

  // Equivalent plastic strain for visualisation
  CellFunction<double> eps_eq(mesh);

  // Load-disp info
  unsigned int step = 0;
  while (t < T)
  {
    t += dt;
    step++;
    std::cout << "step begin: " << step << std::endl;
    std::cout << "time: " << t << std::endl;

    // Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem, *u.vector());

    // Update variables
    constitutive_update->update_history();

    // Write output to files
    file1 << u;
    constitutive_update->eps_p_eq().compute_mean(eps_eq);
    file2 << eps_eq;
  }

  cout << "Solution norm: " << u.vector()->norm("l2") << endl;

  timer.stop();
  dolfin::list_timings();

  return 0;
}
