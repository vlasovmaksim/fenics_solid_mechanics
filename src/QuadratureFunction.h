// Copyright (C) 2009-2012 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2009-10-02
// Last changed: 2012-07-17

#ifndef __QUADRATURE_FUNCTION_H
#define __QUADRATURE_FUNCTION_H

// modified 19.07.2015 to compile VS2013 project
#include "fenics_solid_mechanics_export.h"


#include <memory>
#include <vector>
#include <Eigen/Dense>
#include <boost/multi_array.hpp>

#include <dolfin/function/GenericFunction.h>

namespace dolfin
{
  class Cell;
  class FiniteElement;
  class Function;
  class Mesh;
  template<typename T> class CellFunction;
}

namespace ufc
{
  class cell;
}

namespace fenicssolid
{

  class FENICSSOLIDMECHANICSAPI StateUpdate;

  // Class to ease the handling of integration point values

  class FENICSSOLIDMECHANICSAPI QuadratureFunction : public dolfin::GenericFunction
  {
  public:

    /// Constructor
    QuadratureFunction(const dolfin::Mesh& mesh,
                       std::shared_ptr<const dolfin::FiniteElement> element,
                       const std::vector<double>& w);

    /// Constructor
    QuadratureFunction(const dolfin::Mesh& mesh,
                       std::shared_ptr<const dolfin::FiniteElement> element,
                       std::shared_ptr<StateUpdate> state_updater,
                       const std::vector<double>& w);

    /// Destructor
    ~QuadratureFunction();

    // -- GenericFunction interface

    std::size_t value_rank() const
    { return _element->value_rank(); }

    std::size_t value_dimension(std::size_t i) const
    { return _element->value_dimension(i); }

    void restrict(double* w,
                  const dolfin::FiniteElement& element,
                  const dolfin::Cell& cell,
                  const double* coordinates,
                  const ufc::cell& ufc_cell) const;

    void compute_vertex_values(std::vector<double>&,
                               const dolfin::Mesh&) const;

    // -- QuadratureFunction extensions

    /// Compute average value per cell for scalar data
    void compute_mean(dolfin::CellFunction<double>& mf) const;

    std::shared_ptr<const dolfin::FiniteElement> element() const
    { return _element; }

  private:

    // Finite element
    std::shared_ptr<const dolfin::FiniteElement> _element;

    std::shared_ptr<StateUpdate> _state_updater;

    // Data storage
    boost::multi_array<double, 3> _data;

    // Coefficient
    const std::vector<double>& _w;

  };

}

#endif
