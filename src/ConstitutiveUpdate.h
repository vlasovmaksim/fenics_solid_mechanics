// Copyright (C) 2006-2010 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2010-01-03

#ifndef __CONSTITUTIVE_UPDATE_H
#define __CONSTITUTIVE_UPDATE_H

// modified 19.07.2015 to compile VS2013 project
#include "fenics_solid_mechanics_export.h"


#include <vector>
#include <Eigen/Dense>

#include <boost/multi_array.hpp>
#include "HistoryData.h"
#include "ReturnMapping.h"
#include "StateUpdate.h"

namespace dolfin
{
  class Cell;
  class FiniteElement;
  class Function;
  class GenericDofMap;
}

namespace fenicssolid
{

  class FENICSSOLIDMECHANICSAPI PlasticityModel;

  class FENICSSOLIDMECHANICSAPI ConstitutiveUpdate : public StateUpdate
  {
  public:

    /// Constructor
    ConstitutiveUpdate(const dolfin::Function& u,
                       const dolfin::FiniteElement& sigma_element,
                       const dolfin::GenericDofMap& stress_dofmap,
                       const PlasticityModel& plastic_model);

    /// Destructor
    ~ConstitutiveUpdate();

    /// Update stress for cell
    void update(const dolfin::Cell& cell,
                const double* vertex_coordinates);

    /// Update history variables
    void update_history();

    const std::vector<double>& w_stress() const
    { return _w_stress; }

    const std::vector<double>& w_tangent() const
    { return _w_tangent; }

    const HistoryData& eps_p_eq() const
    { return _eps_p_equiv; }

  private:

    // Restriction to UFC cell
    std::vector<double> _w_stress;
    std::vector<double> _w_tangent;

    // Geometric dimension
    const std::size_t _gdim;

    // Displacement field
    const dolfin::Function& _u;

    // Plasticity model
    const PlasticityModel& _plastic_model;

    // Return mapping object
    ReturnMapping return_mapping;

    // Internal variables
    HistoryData _eps_p;
    HistoryData _eps_p_equiv;

    // Track points that deformed plastically at end of last increment
    boost::multi_array<bool, 2> _plastic_last;

    std::size_t num_ip_per_cell;

    // Basis function derivatives on reference elemennt
    boost::multi_array<double, 3> _derivs_reference;

    // Scratch data
    std::vector<double> _expansion_coeffs;

    // Elastic tangent
    const Eigen::Matrix<double, 6, 6>& De;

  };
}

#endif
