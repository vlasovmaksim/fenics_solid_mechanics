#ifndef __FENICS_SOLID_MECHANICS_EXPORT_H
#define __FENICS_SOLID_MECHANICS_EXPORT_H

#ifdef USEFENICSSOLIDMECHANICSLIBRARY
#ifdef  FENICSSOLIDMECHANICSLIBRARY_EXPORTS 
#define FENICSSOLIDMECHANICSAPI __declspec(dllexport)
#else
#define FENICSSOLIDMECHANICSAPI __declspec(dllimport)
#endif
#else
#define FENICSSOLIDMECHANICSAPI
#endif


#endif // __FENICS_SOLID_MECHANICS_EXPORT_H