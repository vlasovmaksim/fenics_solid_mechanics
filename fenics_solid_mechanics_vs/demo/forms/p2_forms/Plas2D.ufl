#!/usr/bin/env python
# Copyright (C) 2009-2011 Kristian B. Oelgaard and Garth N. Wells.
# Licensed under the GNU LGPL Version 3.
#
# First added:  2009-08-21
# Last changed: 2011-02-06
#

elementA = VectorElement("Lagrange", triangle, 2)
elementT = VectorElement("Quadrature", triangle, 2, 9)
elementS = VectorElement("Quadrature", triangle, 2, 3)

v = TestFunction(elementA)
u = TrialFunction(elementA)
f = Coefficient(elementA)
t = Coefficient(elementT)
s = Coefficient(elementS)

# eps_xx, eps_yy, eps_zz, gam_xy, gam_xz, gam_yz
def eps(u):
    return as_vector([u[i].dx(i) for i in range(2)] + [u[i].dx(j) + u[j].dx(i) for i, j in [(0, 1)]])

# xx, xy, yx, yy
def sigma(s):
    return as_matrix([[s[0], s[2]], [s[2], s[1]]])

def tangent(t):
    return as_matrix([[t[i*3 + j] for j in range(3)] for i in range(3)])

a = inner(eps(v), dot(tangent(t), eps(u)) )*dx
L = inner(grad(v), sigma(s))*dx - inner(v, f)*dx
